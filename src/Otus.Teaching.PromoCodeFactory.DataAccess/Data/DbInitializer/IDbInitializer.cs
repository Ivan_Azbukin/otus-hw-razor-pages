﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.DbInitializer
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}