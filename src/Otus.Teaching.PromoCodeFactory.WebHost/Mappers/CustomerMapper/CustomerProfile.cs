using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers.CustomerMapper
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        { 
            CreateMap<Customer, CustomerShortResponse>();
            
            CreateMap<Customer, CustomerResponse>()
                .ForMember(dst => dst.Preferences,
                    opt => opt.Ignore())
                .ForMember(dst => dst.PromoCodes,
                    opt => opt.Ignore());
            
            CreateMap<IEnumerable<PreferenceShortResponse>, CustomerResponse>()
                .ForMember(dst => dst.Preferences,
                    opt =>
                        opt.MapFrom(src =>
                            new List<PreferenceShortResponse>(src)))
                .ForAllOtherMembers(opt => opt.Ignore());
            
            CreateMap<IEnumerable<PromoCodeShortResponse>, CustomerResponse>()
                .ForMember(dst => dst.PromoCodes,
                    opt =>
                        opt.MapFrom(src => new List<PromoCodeShortResponse>(src)))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(dst => dst.FullName, opt => opt.Ignore())
                .ForMember(dst => dst.PromoCodes, opt => opt.Ignore())
                .ForMember(dst => dst.CustomerPreferences, opt => opt.Ignore());

            CreateMap<IEnumerable<CustomerPreference>, Customer>()
                .ForMember(dst => dst.CustomerPreferences,
                    opt =>
                        opt.MapFrom(src =>
                            new List<CustomerPreference>(src)))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}